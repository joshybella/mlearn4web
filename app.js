/**
 * Module dependencies.
 */
var express = require('express'),
    mongoose = require('mongoose'),
    MongoStore = require('connect-mongo')(express),
    ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn,
    flash = require('connect-flash');
var routes = require('./routes'), //routes
    authoring = require('./routes/authoring'),
    mobile = require('./routes/mobile'),
    login = require('./routes/login'),
    api = require('./routes/api'),
    registration = require('./routes/registration'),
    visualization = require('./routes/visualization');
var models = require('./models'),
    Scenario,      //Scenario mongoose model
    CollectedData, //Collected Data mongoose model
    User;          //User mongoose model
var http = require('http');
var path = require('path');

/**
 * Database configuration
 */
var conf = {
    db: {
        db: 'letsgo4web', //mlearn4web
        host: 'localhost',
        port: 27017  // optional, default: 20762
        //username: 'mlearn', // optional
        //password: 'm1234', // optional
        //collection: 'mySessions' // optional, default: sessions
    },
    secret: 'ubersecret'
};

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon(__dirname + '/public/img/favicon.ico'));
app.use(express.logger('dev'));
app.use(express.bodyParser({keepExtensions: true, uploadDir: './public/files'}));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.cookieParser('supersecret'));
app.use(express.session({
    secret: conf.secret,
    maxAge: new Date(Date.now() + 3600000),
    store: new MongoStore(conf.db)
}));
app.use(flash());
app.use(login.getPassport().initialize());
app.use(login.getPassport().session());
app.use(express.static(path.join(__dirname, 'public')));
app.use(app.router);
app.use(require('less-middleware')({ src: __dirname + '/public' }));



// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}

/**
 * Models for the database and exports
 */
models.defineModels(mongoose, function () {
    var dbUrl = 'mongodb://';
    //dbUrl += conf.db.username+':'+conf.db.password+'@';
    dbUrl += conf.db.host + ':' + conf.db.port;
    dbUrl += '/' + conf.db.db;
    app.Scenario = Scenario = mongoose.model('Scenario');
    app.CollectedData = CollectedData = mongoose.model('CollectedData');
    app.User = User = mongoose.model('User');
    db = mongoose.connect(dbUrl);
});

/**
 * exports models to use in other "classes"
 */
exports.newUser = function(params){
    var u = new User(params);
    return u;
}
exports.getScenario = function(){
    var s = Scenario;
    return s;
}
exports.getCollectedData = function(){
    var c = CollectedData;
    return c;
}
exports.getUser = function(){
    var u = User;
    return u;
}

/**
 * routes
 */
app.get('/', routes.index);

//authoring
app.get('/letsgo4web', ensureLoggedIn('/login'), authoring.scenarios); //the authoring tool
app.post('/letsgo4web/savescenario', authoring.save); //save a scenario as JSON
app.get('/letsgo4web/:sid?', authoring.index); //lets the user chose from a list of scenarios or create one

//mobile
app.get('/mobile', mobile.index);
app.post('/mobile/savedata', mobile.save); //save the collected data as JSON
app.get('/mobile/showdata', mobile.data); //save the collected data as JSON
app.get('/mobile/:sid?', mobile.index); //lets the user chose from a list of scenarios or creates a default one

//visualization
app.get('/visualization/:sid?', visualization.index);

//testing purposes
app.get('/test', mobile.test);

//API
//get all scenarios as JSON
app.get('/mlearn4web/getall', api.getall); //get the json data of all scenarios
//get information about a certain scenario as JSON
app.get('/mlearn4web/get/:sid?', api.getscenario); //get the json data of a scenario
//add a new scenario
app.post('/mlearn4web/newscenario', api.newscenario); //create a new scenario
//update a scenario
app.put('/mlearn4web/updatescenario/:sid?', api.updatescenario);
//delete a scenario
app.get('/mlearn4web/deletescenario/:sid?', api.deletescenario);

//get all data as JSON
app.get('/mlearn4web/getalldata', api.getalldata);
//get a certain dataset as JSON
app.get('/mlearn4web/getdata/:did?', api.getdata);
//get all saved data from a certain scenario as JSON
app.get('/mlearn4web/getscenariodata/:sid?', api.getscenariodata); //get the json of the saved data of a scenario
//post a new dataset to a certain scenario
app.post('/mlearn4web/newdata/:sid?', api.newdata);
//update a certain dataset
app.put('/mlearn4web/updatedata/:did?', api.updatedata);
//delete a certain dataset
app.get('/mlearn4web/deletedata/:did?', api.deletedata);

//get all media as JSON
app.get('/mlearn4web/getuserdata/:uid?', api.getuserdata); //get the json of the saved data of a scenario

//get a certain media file
app.get('/mlearn4web/getallmedia', api.getallmedia); //get the json data of all saved media files
//get all saved data as JSON
app.get('/mlearn4web/getmedia/:mid?', api.getmedia); //get the json data of all saved media files
//update a certain media file


//mobile api
app.get('/getdata', mobile.getdata); //get the json data of a scenario


//registration
app.get('/register', registration.index);
app.post('/register/newuser', registration.register);

//login
login.initializeLogin();

app.get('/login', login.index, routes.index);
app.get('/logout', login.loggedIn, login.logout);
// app.get('/login/failure', login.failure); 
// app.get('/login/success', login.success);
app.post('/auth/local', login.local);
// Redirect the user to Facebook for authentication.  When complete, Facebook
// will redirect the user back to the application at
//   /auth/facebook/callback
app.get('/auth/facebook', login.facebook);
// Facebook will redirect the user to this URL after approval. 
app.get('/auth/facebook/callback', login.facebookCallback);
// Redirect the user to Twitter for authentication.  When complete, Twitter
// will redirect the user back to the application at
//   /auth/twitter/callback
app.get('/auth/twitter', login.twitter);
// Twitter will redirect the user to this URL after approval. 
app.get('/auth/twitter/callback', login.twitterCallback);
// Redirect the user to Google for authentication.  When complete, Google
// will redirect the user back to the application at
//     /auth/google/callback
app.get('/auth/google', login.google);
// Google will redirect the user to this URL after authentication.  
app.get('/auth/google/callback', login.googleCallback);

//app.get('/users', user.list);

http.createServer(app).listen(app.get('port'), function(){
    console.log('Express server listening on port ' + app.get('port'));
});
