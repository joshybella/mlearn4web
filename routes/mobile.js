var letsgo = require('../app');

/*
 * GET mobile.
 */
exports.index = function(req, res) {
    var scenarioFromDB = letsgo.getScenario();

    var sId = req.params.sid;

    console.log(sId);

    if (sId) {
	    scenarioFromDB.findOne({ _id: sId }, function (err, scenario) {
  			if (err) return console.error(err);
  			console.log(scenario);

            if (!scenario) {
                showList(res, 'Scenario not available.');
                return;
            }

  			res.render('mobile', {
		        scenario: scenario
		    });
		});	
	} else {
	   showList(res);
	}
};

exports.data = function(req, res) {
    var collectedDataFromDB = letsgo.getCollectedData();

	collectedDataFromDB.find(function (err, collectedData) {
			if (err) return console.error(err);
			res.end('' + collectedData);
	});	
};

exports.save = function(req, res) {

    var scenarioId = decodeURI(req.headers.referer.substring(req.headers.referer.lastIndexOf('/') + 1));

	var data = {
		'scenarioId': '',
        'groupname': '',
        'data': {}
	}

    console.log(req.body);
	
	for (var element in req.body) {
        if (element === 'scenarioid') {
            data['scenarioId'] = req.body['scenarioid'];
        } else if (element === 'gn') {
            data['groupname'] = req.body['gn'];
        } else {
    		var parts = element.split('_');
            var screenId = parts[0];
            if (!data.data[screenId]) {
                data.data[screenId] = [];
            }

            var dataElement = {
                'elementId': parts[1],
                'type': '',
                'value': ''
            }

            var elem = parts[1].substring(0,2);

    		var type;
    		switch (elem) {
    			case 'tf': 
    				type = 'textfield';
                	break;
                case 'nf': 
    				type = 'numerical';
                	break;
                case 'df': 
    				type = 'date';
                	break;
                case 'ta': 
    				type = 'textarea';
                	break;
                case 'cb': 
    				type = 'choice';
                	break;
                case 'cl': 
    				type = 'location';
                	break;
                case 'co': 
    				type = 'orientation';
                	break;
                case 'qr': 
    				type = 'qrreader';
                	break;
                case 'gn':
                    type = 'groupname';
                    break;
                default:
                	type = 'unknown';
                	break;
    		}

            dataElement.type = type;
            dataElement.value = req.body[element];

    		data.data[screenId].push(dataElement);
        }
	}

	for (var element in req.files) {
		var parts = element.split('_');
        var screenId = parts[0];
        if (!data.data[screenId]) {
            data.data[screenId] = [];
        }


        var dataElement = {
            'elementId': parts[1],
            'type': '',
            'value': ''
        }

        var elem = parts[1].substring(0,2);
        
        var path = req.files[element].path;
        if (path.indexOf('public/') === 0) {
            path = path.substring(6);
        }

		var type;
        switch (elem) {
			case 'ci': 
				type = 'image';
            	break;
            case 'cv': 
				type = 'video';
            	break;
            case 'ca': 
				type = 'sound';
            	break;
            default:
            	type = 'unknown';
            	break;
		}

        dataElement.type = type;
        dataElement.value = path;

        data.data[screenId].push(dataElement);
	}

	var CollectedData = letsgo.getCollectedData(),
        c = new CollectedData(data);

    c.save(function(err) {
        if (err){
            console.log(err);
            console.log('Collected Data save failed');
            res.end('Collected Data saved failed');
            return;
        } else {
            console.log('Collected Data saved');
            showList(res, 'Data was successfully submitted.');
        }
    });
};

/*
 * Call to get the json of a certain scenario (req.params.sname).
 */
exports.getdata = function(req, res) {
  var scenarioFromDB = letsgo.getScenario();
  var sName = req.params.sname;

  scenarioFromDB.find(function (err, scenarios) {
    res.json(scenarios);
  });
  /*
  scenarioFromDB.findOne({ title: sName }, function (err, scenario) {
    res.json(scenario);
  });*/
};

exports.test = function(req, res) {
  res.render('test');
};

function showList(res, message) {
    var scenarioFromDB = letsgo.getScenario();

    scenarioFromDB.find()
                    .sort({'lastModified': -1})
                    .populate('user', 'username') //get the username of the user who created the scenario
                    .exec(function (err, scenarios) {
                        if (err) return console.error(err);
                        res.render('mobile_list', {
                            scenarios: scenarios,
                            message: message
                        });
                    }); 
};

