$( document ).ready(function() {
	var userID = $("#user").attr("uid");

	$("#bt_new").click(function(){
	ga('send', 'event', 'user_action', 'new_scenario', userID);
	});

	$(".list-group-item").click(function(){
	ga('send', 'event', 'user_action', 'edit_scenario', userID);
	});

	$(document).on('click', '.remove-scenario', function(event) {
		event.preventDefault();
	});

	$(document).on('click', '#remove-scenario-button', function() {
		var id = $(this).attr('data-id');
		deleteScenario(id);
		// $('#remove-scenario-dialog').modal('hide');
	});

	$('#remove-scenario-dialog').on('show.bs.modal', function (event) {
	  var button = $(event.relatedTarget) // Button that triggered the modal
	  var id = $(button).parent('a').attr('data-id');
	  var title = $(button).parent('a').attr('data-title');

	  //console.log("ID: " + id);
	  //console.log("TITLE: " + title);

	  //var recipient = button.data('whatever') // Extract info from data-* attributes
	  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
	  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
	  var modal = $(this);
	  modal.find('#remove-scenario-title').text(title);
	  modal.find('#remove-scenario-button').attr('data-id', id);
	});

	$('#scenarios table').tablesorter({
		sortList: [[0,0]],
		headers: { 
            3: { sorter: false }
        } 
	}); 
	$('#otherScenarios table').tablesorter({
		sortList: [[3,0]],
		headers: { 
            4: { sorter: false }
        } 
	}); 

});

function deleteScenario(id) {
    $.get('/mlearn4web/deletescenario/' + id, function() {
    	location.reload();
    });
}