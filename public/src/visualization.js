var visApp = angular.module('visApp', []);

visApp.controller('VisCtrl', function ($scope, $http) {
  var scenarioid = document.getElementById('header').getAttribute('data-scenarioid');
  console.log(scenarioid);


  $http.get('/mlearn4web/getscenariodata/' + scenarioid).
    success(function(data, status, headers, config) {
      //console.log(data);

      var dataGroups = [];
      var dataScreens = [];
      var dataItems = [];
      for (var i = 0; i < data.length; i++) {
        //console.log(data[i]);
        var group = data[i].groupname;
        var indexGroup = dataGroups.indexOf(group);
        if (indexGroup === -1) {
          dataGroups.push(group);
          dataItems[group] = [];
        }
        for (scr in data[i].data) {
          var indexScreen = dataScreens.indexOf(scr);
          if (indexScreen === -1) {
            dataScreens.push(scr);
          }
        }
        dataItems[group].push(data[i].data);
      }

      $scope.data = dataItems;
      $scope.screens = dataScreens;
      $scope.groups = dataGroups;

      console.log(dataItems);
    }).
    error(function(data, status, headers, config) {
      console.log('Error: ' + status + ' ' + data);
    });
});


var map_init = false;

$(function() {

  // $(document).on('click', '.top-nav-item', function() {
  //  		var $this = $(this);
  //  		$('.top-nav-item.active').removeClass('active');
  //  		$(this).addClass('active');
  // });


  $(document).on('click', '.nav-item', function() {
    var groupname = $(this).text();
    $('#main-content > div').each(function() {
      var $this = $(this);
      if ($this.attr('data-group') === groupname) {
        $this.removeClass('hidden');
      } else {
        $this.addClass('hidden');
      }
    });
    if (!map_init) initialize();
  });


  function initialize() {
    map_init = true;
    $('.map-canvas').each(function(index, item) {
      var location = $(item).attr('data-location');
      var semicolon = location.indexOf(';');
      var lat = location.substring(0, semicolon);
      var lon = location.substring(semicolon + 1);
      var latlon = new google.maps.LatLng(lat, lon);
      var map;
      var mapOptions = {
        zoom: 13,
        center: latlon
      };
      map = new google.maps.Map(item,
        mapOptions);
      var marker = new google.maps.Marker({
        position: latlon,
        map: map
      });
    });
  }


});
