/**
 * Created by joshy on 12/9/14.
 */

var visualization = angular.module('Visualization', ['uiGmapgoogle-maps']);

visualization
  .config(function(uiGmapGoogleMapApiProvider) {
    uiGmapGoogleMapApiProvider.configure({
      //key: 'AIzaSyBdUO4IupvYhgqmQyTUkCHlIewdYpOCWHM',
      v: '3.17',
      libraries: 'weather,geometry,visualization'
    });
  })

  .controller('VisCtrl', function($scope, uiGmapGoogleMapApi){
    $scope.groups = groupnames;
    $scope.scenariodata = scenariodata;
    $scope.scenarioname = scenario.title;

    $scope.screens = [];
    $scope.scr = {};

    /**
     * Filter out the amount of unique screens and store them in a list
     */
    scenariodata.forEach(function(item){
      var obj = Object.keys(item.data);

      obj.forEach(function(entry, i){
        if ($scope.screens.indexOf(entry) === -1){
          $scope.screens.push(entry);

          //create maps for every screen entry

          var mapname = "map"+(i+1),
              markername = "markers"+(i+1);

          $scope[markername] = [];

          $scope.scr[mapname] = {
            name : mapname

          };

          $scope[mapname] = {
            center: { latitude: 56.855670, longitude: 14.828869 },
            zoom: 14,
            bounds: {},
            id: i
          };
        }
      });
    });

    $scope.getBounds = function(i) {
      var mapname = "map"+(i+1);
      return $scope[mapname]["bounds"];
    };

    $scope.getCenter = function(i) {
      var mapname = "map"+(i+1);
      return $scope[mapname]["center"];
    };

    /*
    $scope.getZoom = function(i) {
      var mapname = "map"+(i+1);
      return $scope[mapname]["zoom"];
    };*/

    $scope.getMarkers = function(i) {
      var markername = "markers"+(i+1);
      return $scope[markername];
    };

    var createRandomMarker = function(i, bounds, idKey) {
      var lat_min = bounds.southwest.latitude,
        lat_range = bounds.northeast.latitude - lat_min,
        lng_min = bounds.southwest.longitude,
        lng_range = bounds.northeast.longitude - lng_min;

      if (idKey == null) {
        idKey = "id";
      }

      var latitude = lat_min + (Math.random() * lat_range);
      var longitude = lng_min + (Math.random() * lng_range);
      var ret = {
        latitude: latitude,
        longitude: longitude,
        title: 'm' + i
      };
      ret[idKey] = i;
      return ret;
    };

    $scope.randomMarkers = [];

    $scope.$watch(function() {
      return $scope.map1.bounds;

    }, function(nv, ov) {
      // Only need to regenerate once
      if (!ov.southwest && nv.southwest) {
        var markers = [];
        for (var i = 0; i < 3; i++) {
          markers.push(createRandomMarker(i, $scope.map1.bounds))
        }
        $scope.randomMarkers = markers;
      }
    }, true);

    uiGmapGoogleMapApi.then(function(maps) {


      $scope.markers = [];

      scenariodata.forEach(function(item){
        var d = item.data;

        $scope.screens.forEach(function (e, count) {
          if (d[e]){
            d[e].forEach(function(elem){
              if (elem.type == "location"){
                var coords = elem.value;
                var values = {};
                var val_list = [];

                d[e].forEach(function(ele, c){
                  if (ele.type != "location"){
                    values["value"+(c+1)] = {
                      value: ele.value
                    }
                    val_list.push("value"+(c+1));
                  }
                });

                var marker = {
                  id: Math.random(),
                  latitude: coords.split(";")[0], longitude: coords.split(";")[1],
                  group: item.groupname,
                  value: values,
                  screen: e
                };

                console.log(values);

                val_list.forEach(function(el, ct){
                  //marker["value"+(ct+1)]["value"] = el;
                });



                var markersname = "markers"+(count+1);

                $scope[markersname].push(marker);
              }
            });
          }
        });
      });
    });
  });
