'use strict';

angular.module('famousAngularStarter')
  .controller('MainCtrl', function ($scope, $famous) {
    var Engine                      = $famous["famous/core/Engine"];
    var Surface                     = $famous["famous/core/Surface"];
    var Modifier                    = $famous["famous/core/Modifier"];
    var HeaderFooterLayout          = $famous["famous/views/HeaderFooterLayout"];
    var RenderController            = $famous["famous/views/RenderController"];

    var mainContext = Engine.createContext();

    var renderController = new RenderController();
    var surfaces = [];
    var counter = 0;

    var layout = new HeaderFooterLayout({
      headerSize: 50,
      footerSize: 50
    });

    layout.header.add(new Surface({
      size: [undefined, 100],
      content: "Header",
      classes: ["red-bg"],
      properties: {
        lineHeight: "50px",
        textAlign: "center"
      }
    }));

    for (var i = 0; i < 10; i++) {
      surfaces.push(new Surface({
        content: "Surface: " + (i + 1),
        size: [undefined, undefined],
        properties: {
          backgroundColor: "hsl(" + (i * 360 / 10) + ", 100%, 50%)",
          lineHeight: "200px",
          textAlign: 'center'
        }
      }));
    }
    renderController.show(surfaces[0]);

    layout.content.add(
      new Modifier({origin: [.5, .5]})).add(
        renderController
    );

    var lorem = "mLearn4web";
    var fButton = "<a class='ui-btn-right ui-btn ui-btn-b ui-icon-arrow-r ui-btn-icon-notext ui-shadow ui-corner-all' data-transition='slide' href='#screen1' data-role='button' role='button'>Next</a>"
    var content = lorem+fButton;

    layout.footer.add(new Surface({
      size: [undefined, 50],
      content: content,
      classes: ["footer", "ui-footer"],
      properties: {
        lineHeight: "50px",
        textAlign: "center"
      }
    }));

    Engine.on("click", function() {
      var next = (counter++ + 1) % surfaces.length;
      this.show(surfaces[next]);
    }.bind(renderController));

    mainContext.add(layout);


  });
