var settings = {
  db: {
    db: 'letsgo4web', //mlearn4web
    host: 'localhost',
    port: 27017  // optional, default: 20762
    //username: 'mlearn', // optional
    //password: 'm1234', // optional
    //collection: 'mySessions' // optional, default: sessions
  },
  secret: 'ubersecret'
}

module.exports = settings;